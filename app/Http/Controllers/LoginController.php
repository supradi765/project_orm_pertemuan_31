<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Pengguna;

class LoginController extends Controller
{
    public  function formLogin()
    {
        return view('pengguna.form_login');
    }

    public function ProsesLogin(Request $req)
    {
        request()->validate(
            [
                'email' => 'required',
                'password' => 'required',
            ]
        );

        $kredensil = $req->only('email', 'password');

        if (Auth::attempt($kredensil)) {
            $user = Auth::user();
            if ($user->level == 'admin') {
                return redirect()->route('admin.beranda');
            } elseif ($user->level == 'editor') {
                return redirect()->route('editor.beranda');
            }
            return redirect()->intended('/');
        }
        return redirect('master/login')
            ->withInput()
            ->withErrors(['login_gagal' => 'These credentials do not match our records.']);
    }
}
