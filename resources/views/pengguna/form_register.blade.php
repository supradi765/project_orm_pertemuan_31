<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{!! asset('asset/register.css') !!}">
  </head>
  <body>
        {{-- navbar --}}
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand ms-4" href="#">Logo</a>
            <div class="collapse navbar-collapse">
              <ul class="navbar-nav ms-auto me-4">
                <li class="nav-item active">
                  <a class="nav-link" href="#">Beranda</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Tentang</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Kontak</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('login.form-login') }}">Login</a>
                </li>
              </ul>
            </div>
          </nav>
          {{-- navbar selesai --}}

          {{-- form register --}}

        <div class="container ms-4 mt-5">
            <div class="row mb-xl-2">
              <div class="card custom-card mb-xl-5">
                <div class="card-body mb-xl-2">
                    <h3>
                        @if (session('error'))
                            {{ session('error') }}
                            @endif
                            @if(session('sucses'))
                            {{ session('sucses') }}
                            @endif
                    </h3>
                  <h5 class="card-title text-center">Create account</h5>
                  <hr>
                  <form action="{{ route('register.simpan-register') }}" method="POST">
                    @csrf
                    <div class="row mb-2">
                      <div class="col-md-6">
                        <label for="nama" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}" required>
                      </div>
                      <div class="col-md-6">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}">
                      </div>
                    </div>
                    <hr>
                    <div class="row mb-2">
                      <div class="col-md-6">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
                      </div>
                      <div class="col-md-6">
                        <label for="level" class="form-label">Level</label>
                        <select class="form-select" id="level" name="level" value="{{ old('level') }}" required>
                          <option value="admin">Admin</option>
                          <option value="editor">Editor</option>
                        </select>
                      </div>
                    </div>
                    <hr>
                    <div class="mb-2">
                      <label for="password" class="form-label">Password</label>
                      <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" autocomplete="of" required>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-primary">Submit</button>

                  </form>
                </div>
              </div>
            </div>
          </div>
          {{-- form register selesai --}}

          {{-- content --}}
          {{-- content selesai --}}



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
  </body>
</html>
