<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    {{-- <link rel="stylesheet" type="text/css" href="{!! asset('asset/register.css') !!}"> --}}
  </head>
  <style>
        .custom-card {
        border-bottom: 4px solid #2979ff;
        background-color: rgba(0, 0, 0, 0.3);
        box-shadow: 0 0 10px rgba(255, 0, 0, 0.3);
        text-transform: uppercase;
        }
        body {
            background-image: url('/gambar/geeks-2894621_1280.jpg');
        }
        .card-body h5 {
            color:white;
        }
        .card-body form label{
            color:white;
        }
        .card-body form input{
            margin-bottom: 15px;
            border: none;
            background-color: transparent;
            border-bottom: 2px solid #2979ff;
            color: white;
            font-size: 20px;
        }


        input[type="email"],
        input[type="password"] {
            background-color: transparent !important;
            color: white !important;
        }
  </style>
  <body>


          {{-- form login --}}

          <div class="container m-5">
            <div class="row">
              <div class="col-12 mx-auto">
                <div class="card custom-card">
                  <div class="card-body">
                    <h3>
                        @if($errors->any())
                        @foreach($errors->all() as $error)
                          <div class="alert alert-danger" role="alert">
                            {{ $error }}
                          </div>
                        @endforeach
                      @endif

                            @if(session('error'))
                                 <div class="alert alert-danger">
                                {{ session('error') }}
                                 </div>
                             @endif
                    </h3>
                  <h5 class="card-title text-center">Silahkan login</h5>

                  <form action="{{ route('login.proses-login') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="row mb-2">
                      <div class="md-2">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
                      </div>
                    </div>
                    <hr>
                    <div class="mb-2">
                      <label for="password" class="form-label">Password</label>
                      <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" required>
                    </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" id="rememberPasswordCheck"
                                type="checkbox" />
                            <label class="custom-control-label" for="rememberPasswordCheck">Remember
                                password</label>
                        </div>
                    <hr>
                    <button type="submit" class="btn btn-primary">login</button>

                  </form>
                </div>
                </div>
              </div>
            </div>
          </div>
          {{-- form  selesai --}}

          {{-- content --}}
          {{-- content selesai --}}



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
  </body>
</html>
