<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <style>
    /* Tambahkan gaya khusus di sini */

    /* Misalnya: */
    body {
      background-color: #f8f9fa;
    }
    .jumbotron {
      background-color: #007bff;
      color: #fff;
    }
  </style>
  <title>Tampilan Beranda</title>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Logo</a>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Beranda</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Tentang</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Layanan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('logout.proses') }}">Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  <h1> @if(session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif</h1>

<div>
  <div class="jumbotron text-center ">
    <h1>Selamat Datang di Tampilan Editor</h1>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <h3>Fitur 1</h3>
        <p>Deskripsi fitur 1.</p>
      </div>
      <div class="col-md-4">
        <h3>Fitur 2</h3>
        <p>Deskripsi fitur 2.</p>
      </div>
      <div class="col-md-4">
        <h3>Fitur 3</h3>
        <p>Deskripsi fitur 3.</p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <h3>Fitur 1</h3>
        <p>Deskripsi fitur 1.</p>
      </div>
      <div class="col-md-4">
        <h3>Fitur 2</h3>
        <p>Deskripsi fitur 2.</p>
      </div>
      <div class="col-md-4">
        <h3>Fitur 3</h3>
        <p>Deskripsi fitur 3.</p>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
