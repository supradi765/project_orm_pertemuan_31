<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\AlamatController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\EditorController;
use App\Http\Controllers\LogoutController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('beranda');
});

Route::controller(ContactController::class)->name('contact.')->prefix('master/contact')->group(function () {
    Route::get('/', 'contact')->name('contact');
    Route::get('/form', 'form')->name('form-input');
    Route::post('/simpandata', 'simpanData')->name('simpan-data');
    Route::get('/form-edit/{id}', 'formEdit')->name('form-edit');
    Route::post('/edit/{id}', 'editData')->name('simpan-edit');
    Route::get('/delete-data/{id}', 'deleteData')->name('delete-data');
});

Route::controller(AlamatController::class)->name('alamat.')->prefix('master/alamat')->group(function () {
    Route::get('/', 'alamat')->name('alamat');
    Route::get('/form', 'formAlamat')->name('form');
    Route::post('/simpandata', 'simpanData')->name('simpan-data');
    Route::get('/delete-data/{id}', 'deleteData')->name('delete-data');
});

Route::controller(RegisterController::class)->name('register.')->prefix('master/register')->group(function () {
    Route::get('/', 'formRegister')->name('form-register');
    Route::post('/simpan-register', 'simpanRegister')->name('simpan-register');
});

Route::controller(LoginController::class)->name('login.')->prefix('master/login')->group(function () {
    Route::get('/', 'formLogin')->name('form-login');
    Route::post('/proses-login', 'ProsesLogin')->name('proses-login');
});




Route::controller(AdminController::class)->name('admin.')->middleware('cekLogin:admin')->prefix('admin')->group(function () {
    Route::get('/', 'BerandaAdmin')->name('beranda');
});

Route::controller(EditorController::class)->name('editor.')->middleware('cekLogin:editor')->prefix('editor')->group(function () {
    Route::get('/', 'BerandaEditor')->name('beranda');
});

Route::controller(LogoutController::class)->name('logout.')->prefix('logout')->group(function () {
    Route::get('/', 'logout')->name('proses');
});

// Route::prefix('logout')->group(function () {
//     Route::get('/', [LogoutController::class, 'logout'])->name('logout.proses');
// });







// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
